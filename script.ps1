# $in = Read-Host "Please, input inspected directory"
# $out = Read-Host "Now, please input path where to save report (with filename)" 
# Get-ChildItem ${in} -r | Sort-Object -Descending -Property Length | Select-Object -First 10 | Format-Table Extension, @{Label="Summary Value in Mb";Expression={[Math]::Round($_.Length / 1mb, 2)}} | Out-File -Encoding utf8 ${out}




# Get-ChildItem -Path $args[0] -File -Recurse | 
#     Sort-Object -Property Length -Descending |
#     Group-Object Extension | 
#     Select-Object -first 10 @{Name='Extension';Expression={$_.Name}},@{Name='Summary Value in Mb';Expression={[Math]::Round((Get-ChildItem $_.group.fullname | 
#         Sort-Object Length -Descending | 
#         Select-Object -First 1).length/1mb)}} | 
#     Out-file $args[1] -Encoding utf8


    Get-ChildItem $args[0] -Recurse -File| Group-Object Extension|ForEach{
        $_.Group| Sort-Object Length| Select-Object -Last 1(
            "Extension",
            @{l="Summary Value in Mb";e={[UInt32]($_.Length/1mb)}}
        )
    }| Sort-Object "Summary Value in Mb" -Descending | Select-Object -first 10 | Out-file $args[1] -Encoding utf8